insert into usuario (id, clave, nombre) values (1,	'$2a$10$CpUYo6nTY9lkhyGpXIwINeTxd0m2NFSDHnolXPcq9320yAUZxQlV.',	'leo');--la clave es 123
insert into usuario (id, clave, nombre) values (2,	'$2a$10$CpUYo6nTY9lkhyGpXIwINeTxd0m2NFSDHnolXPcq9320yAUZxQlV.',	'soto');--la clave es 123
insert into usuario (id, clave, nombre) values (3,	'$2a$10$CpUYo6nTY9lkhyGpXIwINeTxd0m2NFSDHnolXPcq9320yAUZxQlV.',	'leosoto');--la clave es 123
insert into persona (id_persona, nombre ) values (1, 'leonidas'); 
insert into persona (id_persona, nombre ) values (2, 'christian');
insert into persona (id_persona, nombre ) values (3, 'soto');
insert into persona (id_persona, nombre ) values (4, 'moran');